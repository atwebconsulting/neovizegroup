window.addEventListener('load', function () {

    // basic settings
    var conditionUrlCs = "https://www.neovize.cz/kontakty/podminky-uziti-webu/";
    var domain = "NeoVize.cz";

    // obtain cookieconsent plugin
    var cookieconsent = initCookieConsent();    



    // run plugin with config object
    cookieconsent.run({
        autorun: true,
        revision: 1,
        current_lang: document.documentElement.getAttribute('lang'),
        theme_css: '{PATH}/cookieconsent.css',
        autoclear_cookies: true,
        page_scripts: true,
        cookie_expiration: 365,

        gui_options: {
            consent_modal: {
                layout: 'box',                // box,cloud,bar
                position: 'middle center',           // bottom,middle,top + left,right,center
                transition: 'slide'           // zoom,slide
            },
            settings_modal: {
                layout: 'box',                // box,bar
                transition: 'slide'           // zoom,slide
            }
        },

        onAccept: function (cookie) {
            
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onAccept)!");
        },

        onChange: function (cookie, changed_preferences) {
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onChange)!");
        },

        languages: {
            cs: {
                consent_modal: {
                    title: 'S cookies se žije lépe',
                    description: 'Aby pro vás bylo prohlížení našich stránek příjemnější a byli jste stále v obraze o našich službách, používáme různé cookies. Jsou to jak nutné cookies (nezbytné pro správné fungování webu), tak analytické (pomáhající nám zlepšovat náš web dle vašich preferencí) a marketingové (umožňující nám oslovit vás relevantní nabídkou našich služeb na stránkách partnerů jako je Facebook nebo Seznam). K jejich využití potřebujeme váš souhlas, který dáte kliknutím na <strong>Přijmout všechny</strong>.<br><br>Svůj souhlas můžete kdykoliv odvolat, příp. nastavit souhlas jen s některými z nich kliknutím na "Nastavit souhlasy", či <a id="nastavitnutne" type="button"  class="cc-link">přijmout jen nezbytné</a> . <br><br> Více o zpracování osobních údajů na našich stránkách <a href="' + conditionUrlCs + '" target="_new">naleznete zde</a>.',
                    primary_btn: {
                        text: 'Přijmout všechny',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Nastavit souhlasy',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Nastavení cookies',
                    save_settings_btn: 'Uložit nastavení',
                    accept_all_btn: 'Přijmout vše',
                    cookie_table_headers: [
                        {col1: 'Jméno cookie'},
                        {col2: 'Doména/Služba'},
                        {col3: 'Expirace'},
                    ],
                    blocks: [
                        {
                            title: 'Použité cookies',
                            description: 'Na webu používáme cookies. Můžete si vybrat, které cookies povolíte.'
                        }, {
                            title: 'Nutné cookies',
                            description: 'Tyto cookies jsou opravdu potřebné pro základní funkčnost webu. Bez nich by web nemusel fungovat správně.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: domain,
                                    col3: '12 měsíců',
                                },
                                {
                                    col1: 'navstevaKontakty',
                                    col2: domain,
                                    col3: '12 měsíců',
                                }, 
                                {
                                    col1: 'navsteva',
                                    col2: domain,
                                    col3: '12 měsíců',
                                }
                            ]
                        }, 
                        {
                            title: 'Analytické cookies',
                            description: 'Tyto cookies sbírají informace o průchodu návštěvníka našimi stránkami . Všechny tyto cookies jsou anonymní a nemohu návštěvníka nijak identifikovat. Tyto cookies nejsou nutné pro fungování webu jako takového, ale pomáhají nám web pro vás neustále vylepšovat.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: '^_ga',
                                    col2: 'Google Analytics',
                                    col3: '2 roky',
                                    is_regex: true
                                },
                                {
                                    col1: '_gid',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                },
                                {
                                    col1: '_gat',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                }
                            ]
                        },
                        {
                            title: 'Marketingové cookies',
                            description: 'Cílem těchto cookies je propojit náš web se sociálními a reklamními sítěmi třetích stran jako například Facebook nebo Google Ads. Díky tomuto propojení vám u těchto partnerů může být zobrazována relevantní reklama.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [                                
                                {
                                    col1: '_fbp',
                                    col2: 'Facebook',
                                    col3: '90 dní',
                                },
                                {
                                    col1: 'dpr',
                                    col2: 'Facebook',
                                    col3: '3 dny',
                                },
                                {
                                    col1: 'wd',
                                    col2: 'Facebook',
                                    col3: '7 dní',
                                },                                
                                {
                                    col1: 'datr',
                                    col2: 'Facebook',
                                    col3: '23 měsíců',
                                },
                                {
                                    col1: 'wd',
                                    col2: 'Facebook',
                                    col3: '7 dní',
                                },
                                {
                                    col1: 'spin',
                                    col2: 'Facebook',
                                    col3: '1 den',
                                },
                                {
                                    col1: 'sb',
                                    col2: 'Facebook',
                                    col3: '2 roky',
                                },
                                {
                                    col1: 'tr',
                                    col2: 'Facebook',
                                    col3: '-',
                                },
                                {
                                    col1: 'xs',
                                    col2: 'Facebook',
                                    col3: '3 měsíce',
                                },
                                {
                                    col1: 'c_user',
                                    col2: 'Facebook',
                                    col3: '30 dní',
                                },
                                {
                                    col1: 'NID',
                                    col2: 'Google',
                                    col3: '6 měsíců',
                                },
                                {
                                    col1: 'ANID',
                                    col2: 'Google',
                                    col3: '13 měsíců',
                                },
                                {
                                    col1: 'retargeting',
                                    col2: 'Google',
                                    col3: '',
                                },
                                {
                                    col1: '1P_JAR',
                                    col2: 'Google',
                                    col3: '1 měsíc',
                                },
                                {
                                    col1: 'CONSENT',
                                    col2: 'Google',
                                    col3: '2 roky',
                                },
                                {
                                    col1: 'IDE',
                                    col2: 'DoubleClick',
                                    col3: '14 dní',
                                },
                                {
                                    col1: 'test_cookies',
                                    col2: 'DoubleClick',
                                    col3: '1 den',
                                },
                                {
                                    col1: 'RUL',
                                    col2: 'DoubleClick',
                                    col3: '-',
                                },
                                {
                                    col1: 'sid',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '1 měsíc',
                                },
                                {
                                    col1: 'APNUID',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '6 měsíců',
                                },
                                {
                                    col1: 'KADUSERCOOKIE',
                                    col2: 'Seznam.cz, a.s.',
                                    col3: '6 měsíců',
                                }
                            ]
                        },                       
                        {
                            title: 'Více informací',
                            description: 'Více informací o zpracování osobních údajů na našich stránkách najdete <a class="cc-link" target="_blank" href="' + conditionUrlCs + '">zde</a>.',
                        }
                    ]
                }
            },
            pl: {
                consent_modal: {
                    title: 'Z cookies żyje się lepiej',
                    description: 'Aby przyjemniej przeglądało się Państwu nasze strony internetowe oraz by byli Państwo na bieżąco z naszymi usługami, korzystamy z różnorodnych cookies. Obowiązkowe cookies są konieczne do prawidłowego działania strony internetowej, cookies analityczne pomagają nam dostosować naszą stronę do Państwa preferencji, a cookies marketingowe pozwalają nam kierować do Państwa dostosowaną ofertę naszych usług na stronach partnerów (np. Facebook lub Seznam). Abyśmy mogli z nich korzystać, potrzebujemy Państwa zgody, której można udzielić, klikając przycisk Przyjąć wszystkie. W dowolnym momencie mogą Państwo wycofać swoją zgodę lub <a id="nastavitnutne" type="button"  class="cc-link">wyrazić zgodę na tylko obowiązkowe cookies</a>.',
                    primary_btn: {
                        text: 'Przyjąć wszystkie',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Konfiguracja zgód',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Ustawienia plików cookies',
                    save_settings_btn: 'Zapisz ustawienia',
                    accept_all_btn: 'Przyjąć wszystkie',
                    cookie_table_headers: [
                        {col1: 'Nazwa pliku cookie'},
                        {col2: 'Domena'},
                        {col3: 'Wygaśnięcie'},
                    ],
                    blocks: [
                        {
                            title: 'Wykorzystywane pliki cookies',
                            description: 'Na naszej stronie internetowej korzystamy z plików cookies. Mogą Państwo wybrać, na które pliki wyrażają Państwo zgodę.'
                        }, {
                            title: 'Obowiązkowe pliki cookies',
                            description: 'Te pliki cookies są naprawdę konieczne do podstawowego funkcjonowania strony internetowej. Bez nich strona internetowa mogłaby nie działać prawidłowo.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
                            cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: domain,
                                    col3: '12 miesięcy',
                                },
                                {
                                    col1: 'navstevaKontakty',
                                    col2: domain,
                                    col3: '12 miesięcy',
                                }, 
                                {
                                    col1: 'navsteva',
                                    col2: domain,
                                    col3: '12 miesięcy',
                                }
                            ]
                        }, 
                        {
                            title: 'Analityczne pliki cookies',
                            description: 'Te pliki cookies gromadzą informacje na temat przepływu odwiedzających przez nasze strony. Analityczne pliki cookies są anonimowe i nie można na ich podstawie w żaden sposób zidentyfikować odwiedzającego. Nie są konieczne do samego działania strony internetowej, ale pomagają nam stale poprawiać działanie strony.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [
                                {
                                    col1: '^_ga',
                                    col2: 'Google Analytics',
                                    col3: '2 lata',
                                    is_regex: true
                                },
                                {
                                    col1: '_gid',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                },
                                {
                                    col1: '_gat',
                                    col2: 'Google Analytics',
                                    col3: '1 den',
                                }
                            ]
                        },
                        {
                            title: 'Marketingowe pliki cookies',
                            description: 'Celem marketingowych plików cookies jest powiązanie naszej strony internetowej z serwisami społecznościowymi i reklamowymi podmiotów zewnętrznych, takich jak Facebook lub Google Ads. Takie powiązanie sprawia, że mogą być Państwu wyświetlane adekwatne reklamy u tych partnerów.',
                            toggle: {
                                value: 'ads',
                                enabled: false,
                                readonly: false
                            },
                            cookie_table: [                                
                                {
                                    col1: '_fbp',
                                    col2: 'Facebook',
                                    col3: '90 dni',
                                },
                                {
                                    col1: 'dpr',
                                    col2: 'Facebook',
                                    col3: '3 dni',
                                },
                                {
                                    col1: 'wd',
                                    col2: 'Facebook',
                                    col3: '7 dni',
                                },                                
                                {
                                    col1: 'datr',
                                    col2: 'Facebook',
                                    col3: '23 měsíců',
                                },
                                {
                                    col1: 'wd',
                                    col2: 'Facebook',
                                    col3: '7 dni',
                                },
                                {
                                    col1: 'spin',
                                    col2: 'Facebook',
                                    col3: '1 den',
                                },
                                {
                                    col1: 'sb',
                                    col2: 'Facebook',
                                    col3: '2 lata',
                                },
                                {
                                    col1: 'tr',
                                    col2: 'Facebook',
                                    col3: '-',
                                },
                                {
                                    col1: 'xs',
                                    col2: 'Facebook',
                                    col3: '3 miesiące',
                                },
                                {
                                    col1: 'c_user',
                                    col2: 'Facebook',
                                    col3: '30 dni',
                                },
                                {
                                    col1: 'NID',
                                    col2: 'Google',
                                    col3: '6 miesięcy',
                                },
                                {
                                    col1: 'ANID',
                                    col2: 'Google',
                                    col3: '13 miesięcy',
                                },
                                {
                                    col1: 'retargeting',
                                    col2: 'Google',
                                    col3: '',
                                },
                                {
                                    col1: '1P_JAR',
                                    col2: 'Google',
                                    col3: '1 miesiąc',
                                },
                                {
                                    col1: 'CONSENT',
                                    col2: 'Google',
                                    col3: '2 lata',
                                },
                                {
                                    col1: 'IDE',
                                    col2: 'DoubleClick',
                                    col3: '14 dni',
                                },
                                {
                                    col1: 'test_cookies',
                                    col2: 'DoubleClick',
                                    col3: '1 den',
                                },
                                {
                                    col1: 'RUL',
                                    col2: 'DoubleClick',
                                    col3: '-',
                                }                                
                            ]
                        }
                    ]
                }
            },            
            
        }
    });


    var nastavitNutne = document.getElementById("nastavitnutne");

    if( nastavitNutne ){

        nastavitNutne.onclick = function () {
            cookieconsent.hide();
            cookieconsent.accept([]); 
        };

    }    

}); 